/* eslint-disable prettier/prettier */
import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/dist/Feather';


const ListItem = ({ item, deleteItem }) => {
    return (
        <View style={styles.listItem}>
            <View style={styles.listItemView}>
                <Text style={styles.listItemText}>{item.text}</Text>
                <TouchableOpacity style={styles.deleteBtn}>
                    <Icon name="check" size={20} color="green" onPress={() => deleteItem(item.id)} />
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    listItem: {
        padding: 15,
        backgroundColor: '#2F363D',
        marginVertical: 8,
        marginHorizontal: 16,
        borderRadius: 6,
    },
    listItemView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    listItemText: {
        fontSize: 18,
        color: '#fff',
    },
    deleteBtn: {
        padding: 5,
    },
})

export default ListItem;
