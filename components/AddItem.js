/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/dist/Feather';

const AddItem = ({ title, addItem }) => {
    const [text, setText] = useState('');

    const onChange = textValue => setText(textValue);

    const submitAndClear = () => {
        addItem(text)
        setText('')
    }

    return (
        <View style={styles.viewContainer}>
            <TextInput placeholder="Add Item ..." style={styles.input} onChangeText={onChange} value={text} />
            <TouchableOpacity style={styles.btn} onPress={submitAndClear}>
                <Text style={styles.btnText}><Icon name="plus" size={20} /> Add item</Text>
            </TouchableOpacity>
        </View >
    )
}

const styles = StyleSheet.create({
    viewContainer: {
        backgroundColor: 'darkslateblue',
        paddingVertical: 16,
    },
    input: {
        height: 50,
        paddingHorizontal: 8,
        fontSize: 16,
        backgroundColor: '#f5f5f5',
        marginHorizontal: 16,
        marginBottom: 8,
        borderRadius: 6,
    },
    btn: {
        backgroundColor: '#c2bad8',
        padding: 9,
        marginHorizontal: 16,
        borderRadius: 6,
    },
    btnText: {
        color: 'darkslateblue',
        fontSize: 20,
        textAlign: 'center',
    },
})

export default AddItem;